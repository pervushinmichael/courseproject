$(function(){
	$('.menu-button').on('click',function(e){
		e.preventDefault;
		$(this).toggleClass('menu-button-active');
		$('.menu-block-inner').toggleClass('menu-block-inner-active');
	});

	$(".sidebar-menu").on("click", "a", function(event){
		event.preventDefault();
		let id = $(this).attr('href'),
			top = $(id).offset().top;
		$('body, html').animate({
			scrollTop: top
		},1000);
	});

	$(window).on('scroll',()=>{
		if($(this).scrollTop()>=50){
			$('.btt-block').fadeIn(100);
		} else {
			$('.btt-block').fadeOut(100);
		}
	});

	$('.btt-block').on('click', (e)=> {
		e.preventDefault();
		$('html').animate({
			scrollTop: 0
		},500);
	})

})
